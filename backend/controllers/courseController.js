import asyncHandler from "express-async-handler";

import Course from "../models/courseModel.js";

/**
 * @desc		Create new course
 * @route		POST /api/course
 * @access	    private
 */

const createCourse = asyncHandler(async (req, res) => {
  const { courseName, level, description, lecturedates, instructorName } =
    req.body;

  const courseExists = await Course.findOne({ courseName });

  if (courseExists) {
    res.status(400); // Bad request
    throw new Error("Course already exists");
  }

  const course = await new Course({
    courseName,
    level,
    description,
    instructorName,
    lecturedates,
  });

  const createdCourse = await course.save();
  res.status(201).json(createdCourse);
});

/**
 * @desc		Get all courses
 * @route		GET /api/courses
 * @access	private/admin
 */
const getCourse = asyncHandler(async (req, res) => {
  const course = await Course.find({});
  res.json(course);
});

export { createCourse, getCourse };
