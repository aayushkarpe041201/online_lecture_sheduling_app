import bcrypt from "bcryptjs";

const users = [
  {
    name: "Admin",
    email: "admin@example.com",
    password: bcrypt.hashSync("123456", 10),
    isInstructor: false,
    isAdmin: true,
  },
  {
    name: "Ayush karpe",
    email: "aayushkarpe041201@gmail.com",
    password: bcrypt.hashSync("123456", 10),
    isInstructor: false,
    isAdmin: true,
  },
  {
    name: "John Doe",
    email: "john@example.com",
    password: bcrypt.hashSync("123456", 10),
    isInstructor: true,
    isAdmin: false,
  },
  {
    name: "Jane Doe",
    email: "jane@example.com",
    password: bcrypt.hashSync("123456", 10),
    isInstructor: true,
    isAdmin: false,
  },
  {
    name: "Jack Doe",
    email: "jack@example.com",
    password: bcrypt.hashSync("123456", 10),
    isInstructor: true,
    isAdmin: false,
  },
];

export default users;
