


import mongoose from 'mongoose';

// const lectureSchema = mongoose.Schema(
//     {
//         courseName: {
//             type: String,
//             required: true,
//         },
//         instructorName: {
//             type: String,
//             require: true,
//         },
//         lecture_dates: [],
//     }
// )


const courseSchema = mongoose.Schema(
    {
        courseName: {
            type: String,
            required: true,
        },
        instructorName:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
        image: {
            type: String,
            required: false,
        },
        description: {
            type: String,
            required: true,
        },
        level: {
            type: Number,
            required: true,
            default: 0,
        },
        lecturedates: [],
    }
);

const Course = mongoose.model('Course', courseSchema);

export default Course;