import express from "express";

import {
  authUser,
  getUserByID,
  getUserProfile,
  getUsers,
  getinstructors,
} from "../controllers/userController.js";
import { admin, protect } from "../middlewares/authMiddleware.js";

const router = express.Router();

router.route("/").get(protect, admin, getUsers);
router.route("/instructor").get(protect, admin, getinstructors);
router.route("/login").post(authUser);
router.route("/profile").get(protect, getUserProfile);
router.route("/:id").get(protect, admin, getUserByID);

export default router;
