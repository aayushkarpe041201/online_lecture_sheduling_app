import express from "express";

import { admin, protect } from "../middlewares/authMiddleware.js";

import { createCourse, getCourse } from "../controllers/courseController.js";

const router = express.Router();

router.route("/").post(protect, admin, createCourse).get(getCourse);


export default router;
