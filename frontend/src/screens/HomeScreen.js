import { Link as RouterLink, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import React from "react";
import AddLectureScreen from "./AddLectureScreen";
import SheduleScreen from "./SheduleScreen";
import { Text } from "@chakra-ui/react";

function HomeScreen() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  return (
    <>
      {userInfo && userInfo.isAdmin ? (
        <AddLectureScreen />
      ) : (userInfo) ? (
        <SheduleScreen />
      ) : (
        <Text>Please login !</Text>
      )}
    </>
  );
}

export default HomeScreen;
