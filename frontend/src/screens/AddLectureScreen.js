import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Spacer,
  Input,
  Select,
} from "@chakra-ui/react";
import { useEffect, useState, useRef } from "react";
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";


import FormContainer from "../components/FormContainer";
import Message from "../components/Message";

import { listInstructors } from "../action/userActions";
import { createCourse } from "../action/courseAction";

function AddLectureScreen() {
  const dispatch = useDispatch();
	const navigate = useNavigate();

  const InstructorsList = useSelector((state) => state.InstructorsList);
  const { loading, error, instructors } = InstructorsList;

  const courseCreate = useSelector((state) => state.courseCreate);
  const { loading: loadingUpdate, error: errorUpdate, course } = courseCreate;

  const [courseName, setcourseName] = useState("");
  const [level, setlevel] = useState(1);
  const [image, setImage] = useState("");
  const [description, setDescription] = useState("");

  const [instructorName, setinstructorName] = useState("");

  const [lecturedates, setlecturedates] = useState([]);

  const dateInputRef1 = useRef(null);
  const dateInputRef2 = useRef(null);

  function getDatesInRange(startDate, endDate) {
    const d1 = new Date(startDate);
    const d2 = new Date(endDate);

    const dates = [];

    while (d1 <= d2) {
      dates.push(new Date(d1));
      d1.setDate(d1.getDate() + 1);
    }

    return dates;
  }

  const [fromdate, setformdate] = useState(new Date("2022-01-18"));
  const [todate, settodate] = useState(new Date("2022-01-24"));

  useEffect(() => {
    if (instructors) dispatch(listInstructors());
    setlecturedates(getDatesInRange(fromdate, todate));
  }, [dispatch, fromdate, todate]);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(
      createCourse({
        courseName,
        level,
        description,
        instructorName,
        lecturedates,
      })
    );
    navigate('/courseScreen');
  };

  return (
    <Flex w="full" alignItems="center" justifyContent="center" py="5">
      <FormContainer>
        <Heading as="h1" mb="8" fontSize="3xl">
          Add Course
        </Heading>

        {errorUpdate && <Message type="error">{errorUpdate}</Message>}

        {error && <Message type="error">{error}</Message>}

        <form onSubmit={submitHandler}>
          <FormControl id="courseName">
            <FormLabel htmlFor="courseName">courseName : </FormLabel>
            <Input
              id="courseName"
              type="text"
              placeholder="New Course"
              value={courseName}
              onChange={(e) => setcourseName(e.target.value)}
            />
          </FormControl>

          <Spacer h="3" />

          {/* instructor */}
          <FormControl id="instructorName">
            <FormLabel htmlFor="instructorName">instructorName : </FormLabel>

            {instructors && (
              <Flex justifyContent="space-between" py="2">
                <select
                  value={instructorName}
                  onChange={(e) => setinstructorName(e.target.value)}
                  width="30%"
                >
                  <option value="" disabled hidden>
                    Choose a instructor
                  </option>
                  {instructors.map((instructor) => (
                    <option key={instructor._id} value={instructor._id}>
                      {instructor.name}
                    </option>
                  ))}
                </select>
              </Flex>
            )}
          </FormControl>

          <Spacer h="3" />

          {/* IMAGE */}
          {/* <FormControl id="image" isRequired>
            <FormLabel>Image</FormLabel>
            <Input
              type="text"
              placeholder="Enter image url"
              value={image}
              onChange={(e) => setImage(e.target.value)}
            />
            <Spacer h="3" />
            <Input
              type="file"
              // onChange={uploadFileHandler}
            />
          </FormControl> */}

          <Spacer h="3" />

          {/* DESCRIPTION */}
          <FormControl id="description" isRequired>
            <FormLabel>Description</FormLabel>
            <Input
              type="text"
              placeholder="Enter description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            />
          </FormControl>
          <Spacer h="3" />

          {/* Level */}
          <FormControl id="level" isRequired>
            <FormLabel>Level</FormLabel>
            <Input
              type="number"
              placeholder="Level"
              value={level}
              onChange={(e) => setlevel(e.target.value)}
            />
          </FormControl>
          <Spacer h="3" />

          {/* form Date */}
          <FormControl id="fromDate" isRequired>
            <FormLabel>Batch Date from</FormLabel>
            <Input
              type="date"
              onChange={(e) => setformdate(e.target.value)}
              ref={dateInputRef1}
            />
          </FormControl>

          <Spacer h="3" />

          {/* to Date */}
          <FormControl id="toDate" isRequired>
            <FormLabel>to</FormLabel>
            <Input
              type="date"
              onChange={(e) => settodate(e.target.value)}
              ref={dateInputRef2}
            />
          </FormControl>

          <Button
            type="submit"
            colorScheme="teal"
            mt="4"
            isLoading={loadingUpdate}
          >
            Add Course
          </Button>
        </form>
      </FormContainer>
    </Flex>
  );
}

export default AddLectureScreen;
