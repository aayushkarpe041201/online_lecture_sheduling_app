import { Flex, Heading, Spacer, Text } from "@chakra-ui/react";
import React from "react";

import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Loader from "../components/Loader";
import Message from "../components/Message";

import { listCourse } from "../action/courseAction";

function CourselistScreen() {
  const dispatch = useDispatch();

  const courseList = useSelector((state) => state.courseList);
  const { loading, error, courselist } = courseList;

  useEffect(() => {
    dispatch(listCourse());
  }, [dispatch]);
  return (
    <>
      <Heading as="h2" mb="8" fontSize="xl">
        Course List
      </Heading>

      {loading ? (
        <Loader />
      ) : error ? (
        <Message type="error">{error}</Message>
      ) : (
        <Flex
          justifyContent={"space-evenly"}
          direction={"column"}
        >
          {courselist.map((course) => (
            <Flex
              justifyContent={'space-evenly'}
              key={course._id}
              border={"2px solid black"}
              m={'2px'}
            >
              <Text>Course Name: {course.courseName}</Text>
              <Text>Level :{course.level}</Text>
              <Text>Description :{course.description}</Text>
            </Flex>
          ))}
        </Flex>
      )}
    </>
  );
}

export default CourselistScreen;
