import axios from "axios";

import {
  COURSE_CREATE_FAIL,
  COURSE_CREATE_REQUEST,
  COURSE_CREATE_SUCCESS,
  LIST_COURSES_FAIL,
  LIST_COURSES_REQUEST,
  LIST_COURSES_RESET,
  LIST_COURSES_SUCCESS
} from "../constants/courseConstants";

export const createCourse = (course) => async (dispatch, getState) => {
  try {
    dispatch({ type: COURSE_CREATE_REQUEST });
    const {
      userLogin: { userInfo },
    } = getState();
    
    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
    		'Content-Type': 'application/json',
    	},
    };

    const { data } = await axios.post(`/api/course`, course, config);

    dispatch({ type: COURSE_CREATE_SUCCESS, payload: data });
  } catch (err) {
    dispatch({
      type: COURSE_CREATE_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
  }
};

export const listCourse = () => async(dispatch, getState) => {
  try {
    dispatch({type: LIST_COURSES_REQUEST});
    const {
      userLogin: { userInfo }, // 2 level destructuring
		} = getState();
    
		const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
			},
		};
    
    const { data } = await axios.get(`/api/course`, config);
    dispatch({ type: LIST_COURSES_SUCCESS, payload: data });
  } catch (err) {
    dispatch({
      type: LIST_COURSES_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
  }
}