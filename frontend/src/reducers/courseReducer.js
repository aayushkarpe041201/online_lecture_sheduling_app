import {
  COURSE_CREATE_FAIL,
  COURSE_CREATE_REQUEST,
  COURSE_CREATE_SUCCESS,
  COURSE_CREATE_RESET,
  LIST_COURSES_FAIL,
  LIST_COURSES_REQUEST,
  LIST_COURSES_SUCCESS,
  LIST_COURSES_RESET,
} from "../constants/courseConstants";

export const courseCreateReducer = (state = {}, action) => {
  switch (action.type) {
    case COURSE_CREATE_REQUEST:
      return { loading: true };
    case COURSE_CREATE_SUCCESS:
      return { loading: false, success: true, course: action.payload };
    case COURSE_CREATE_FAIL:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const courseListReducer = (state = { courselist: [] }, action) => {
  switch (action.type) {
    case LIST_COURSES_REQUEST:
      return { ...state, loading: true };
    case LIST_COURSES_SUCCESS:
      return { loading: false, courselist: action.payload };
    case LIST_COURSES_FAIL:
      return { loading: false, error: action.payload };
    case LIST_COURSES_RESET:
      return { courselist: [] };
    default:
      return state;
  }
};
