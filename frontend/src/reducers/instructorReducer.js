import {
    USER_INSTRUCTOR_FAIL,
  USER_INSTRUCTOR_REQUEST,
  USER_INSTRUCTOR_RESET,
  USER_INSTRUCTOR_SUCCESS, 
} from '../constants/instructorConstants'

// export const userInstructorReducer = (state = { instructors: [] }, action) => {
//     switch (action.type) {
//       case USER_INSTRUCTOR_REQUEST:
//         return { loading: true, instructors: [] };
//       case USER_INSTRUCTOR_SUCCESS:
//         return { loading: false, instructors: action.payload };
//       case USER_INSTRUCTOR_FAIL:
//         return { loading: false, error: action.payload };
//       default:
//         return state;
//     }
//   };
