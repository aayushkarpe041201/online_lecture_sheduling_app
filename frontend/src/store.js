import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import {
	userDetailsReducer,
	userListReducer,
	userLoginReducer,
	InstructorListReducer
} from './reducers/userReducer';

import {
	courseCreateReducer,
	courseListReducer,
} from './reducers/courseReducer'


const reducer = combineReducers({
	userLogin: userLoginReducer,
	userDetails: userDetailsReducer,
	userList: userListReducer,
	InstructorsList: InstructorListReducer,
	courseCreate: courseCreateReducer,
	courseList: courseListReducer,
});


const userInfoFromStorage = localStorage.getItem('userInfo')
	? JSON.parse(localStorage.getItem('userInfo'))
	: null;


const initialState = {
	userLogin: { userInfo: userInfoFromStorage },
};

const middlewares = [thunk];

const store = createStore(
	reducer,
	initialState,
	composeWithDevTools(applyMiddleware(...middlewares))
);

export default store;
